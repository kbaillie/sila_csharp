@echo off

@rem Directory of this file
SET DIR=%~dp0

@rem Determine OS architecture
SET VERSION=??
wmic os get osarchitecture | FINDSTR /IL "32" > NUL
IF %ERRORLEVEL% EQU 0 SET VERSION=32
wmic os get osarchitecture | FINDSTR /IL "64" > NUL
IF %ERRORLEVEL% EQU 0 SET VERSION=64

IF %VERSION% == 32 GOTO x86
IF %VERSION% == 64 GOTO x64

@rem No versions are found
ECHO OS architecture unknown
GOTO END

:x86
ECHO Using proto compiler for 32 bit OS
SET PLATFORM=windows_x86
GOTO FINISH

:x64
ECHO Using proto compiler for 64 bit OS
SET PLATFORM=windows_x64
GOTO FINISH

:FINISH
@rem set aliases according to current OS architecture
SET PROTOC_PATH=%DIR%sila_tools/Grpc.Tools.1.7.1/tools/%PLATFORM%
SET PROTOC=%PROTOC_PATH%/protoc.exe
SET CSHARP_PLUGIN=protoc-gen-grpc=%PROTOC_PATH%/grpc_csharp_plugin.exe

SET FRAMEWORK_PATH=--proto_path %DIR%sila_base/protobuf

@echo on

:END