using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using NLog;

namespace sila2.Utils
{
    public class ConfigUtils
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger ();
        public static string GetAssemblyPath ()
        {
            return Path.GetDirectoryName (System.Reflection.Assembly.GetExecutingAssembly ().Location);
        }

        public static IConfigurationRoot LoadConfig ()
        {
            var path = GetAssemblyPath ();
            Log.Debug ($"Config path: {path}");
            return new ConfigurationBuilder ()
                .SetBasePath (GetAssemblyPath ())
                .AddXmlFile ("config.xml", optional : true, reloadOnChange : false)
                .Build ();
        }
    }
}