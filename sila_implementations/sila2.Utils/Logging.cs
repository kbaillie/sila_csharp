using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Common.Logging;
using Common.Logging.Configuration;

namespace sila2.Utils
{
    public static class Logging
    {
        private static string GetAssemblyPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        private static IConfiguration LoadConfigXml(string fileName)
        {
            return new ConfigurationBuilder()
                .SetBasePath(GetAssemblyPath())
                .AddXmlFile(fileName, optional: true, reloadOnChange: false)
                .Build();
        }

        /// <summary>
        /// Configures the applications logging, to consume Common.Logging abstractins
        /// as specified by the config file
        /// </summary>
        public static void SetupCommonLogging(string configFile = "common_logging.config")
        {
            var config = LoadConfigXml(configFile);
            var logConfiguration = new LogConfiguration();
            config.GetSection("common:logging").Bind(logConfiguration);
            LogManager.Configure(logConfiguration);
        }
    }
}