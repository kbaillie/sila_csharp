namespace sila2.Utils
{
    using CommandLine;
    using Common.Logging;

    public class ServerCmdLineArgs
    {
        private static ILog Log = LogManager.GetLogger<ServerCmdLineArgs>();

        [Option('p', "port", Default = 50052, HelpText = "Port to connect server to.")]
        public int Port { get; set; }

        [Option('c', "config", Required = false, HelpText = "Path to file to store/load server configuration.")]
        public string ConfigFile { get; set; }

        [Option('i', "interface", Required = true, HelpText = "Network interface/IP Address/ Mask to use for discovery (e.g. lo, wlp1s0, eth0, 192.168.1.0/24, 192.168.1.0)")]
        public string InterfaceNameOrCIDR { get; set; }

        [Option('s', "simulation", Default = false, Required = false, HelpText = "Run the server in simulation")]
        public bool Simulation { get; set; }

        public static void HandleParseError(object errs)
        {
            throw new ServerCmdLineArgsParseException("Failed to parse arguments correctly, try with --help flag");
        }
    }

    public class ServerCmdLineArgsParseException : System.Exception
    {
        public ServerCmdLineArgsParseException() { }
        public ServerCmdLineArgsParseException(string message) : base(message) { }
    }
}