namespace ShakerServer
{
    using Common.Logging;

    public enum ClampState
    {
        Closed,
        Open
    }

    public class ShakerSimulator
    {
        private static readonly ILog _logger = LogManager.GetLogger<ShakerSimulator>();
        private bool shaking = false;
        private ClampState clampState;
        public bool IsShaking
        {
            get
            {
                return shaking;
            }

            set
            {
                _logger.Info($"Shaking: {value}");
                shaking = value;
            }
        }

        public ClampState ClampState
        {
            get
            {
                return clampState;
            }
            set
            {
                _logger.Info($"ClampState: {value}");
                clampState = value;
            }
        }
    }
}