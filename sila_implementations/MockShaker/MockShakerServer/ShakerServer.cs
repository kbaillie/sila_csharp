﻿namespace ShakerServer
{
    using System.IO;
    using System.Threading;
    using System;
    using CommandLine;
    using Common.Logging;
    using sila2.Utils;
    using sila2;

    public class ShakerServer : SiLA2Server
    {
        private static ILog Log = LogManager.GetLogger<ShakerServer>();
        private readonly ShakingControlImpl _shakingControl;
        private readonly PlateHandlingControlImpl _plateHandlingControl;

        public ShakerServer (int portNumber, string interfaceNameOrCIDR, string configFile):
            base (new ServerInformation (
                    "Mock Shaker Server",
                    "Simulates a shaker device with a electronic lock mechanism. In order to shake the lock clamps must be closed",
                    "www.equicon.de",
                    "1.0"),
                portNumber,
                interfaceNameOrCIDR,
                configFile)
            {
                // create SiLA2 Server
                try
                {
                    var shakerSimulator = new ShakerSimulator ();

                    this.ReadFeature ("features/PlateHandlingControl.xml");
                    _plateHandlingControl = new PlateHandlingControlImpl (shakerSimulator);
                    this.GrpcServer.Services.Add (Sila2.De.Equicon.Handling.Platehandlingcontrol.V1.PlateHandlingControl.BindService (_plateHandlingControl));

                    this.ReadFeature ("features/ShakingControl.xml");
                    _shakingControl = new ShakingControlImpl (shakerSimulator, this);
                    this.GrpcServer.Services.Add (Sila2.De.Equicon.Mixing.Shakingcontrol.V1.ShakingControl.BindService (_shakingControl));
                    Log.Info ("Added Services to SiLA2Server");
                }
                catch (Exception ex)
                {
                    Log.Info ("Server creation failed: " + ex.Message);
                }
            }

        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = NLog.LogManager.GetCurrentClassLogger();
            try
            {
                ServerCmdLineArgs options = null;
                Parser.Default.ParseArguments<ServerCmdLineArgs>(args)
                    .WithParsed<ServerCmdLineArgs>(o => options = o)
                    .WithNotParsed<ServerCmdLineArgs>((errs) => ServerCmdLineArgs.HandleParseError(errs));

                // Server Configuration file
                var configFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".sila", "ShakerServer", "config.json");

                // create the shaker server
                ShakerServer shaker = new ShakerServer(options.Port, options.InterfaceNameOrCIDR, options.ConfigFile);
                // start the server
                shaker.StartServer();

                Log.Info("MockShakerDevice server listening on port " + options.Port);

                Thread.Sleep(3000);
                Console.WriteLine("\nPress enter to stop the server...");
                Console.ReadLine();

                shaker.ShutdownServer();
            }
            catch (Exception e)
            {
                Log.Error (e);
            }
        }
    }
}