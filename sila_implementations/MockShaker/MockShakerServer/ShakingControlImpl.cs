﻿namespace ShakerServer
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using System.Xml;
    using Common.Logging;
    using Grpc.Core;
    using sila2;
    using SiLAFramework = Sila2.Org.Silastandard;
    using Sila2.De.Equicon.Mixing.Shakingcontrol.V1;

    internal class ShakingControlImpl : ShakingControl.ShakingControlBase
    {
        private static ILog Log = LogManager.GetLogger<ShakingControlImpl>();
        private readonly ShakerSimulator _shakerSimulator;
        private int _executionTime = 600; // [sec]
        private int _shakingProgressInPercent = 0;
        private bool _progressChanged = false;
        private readonly SiLA2Server _server;

        public ShakingControlImpl(ShakerSimulator shakerSimulator, SiLA2Server server)
        {
            _shakerSimulator = shakerSimulator;
            _server = server;
            _server.CommandExecutionManager.OnCommandRemoved += (id, cmd) =>
            {
                Log.Info($"Command execution {id} removed.");
            };
        }

        #region Overrides of ShakingControlBase

        #region Shake command

        private BackgroundWorker _worker;

        private class WorkerArgs
        {
            public TimeSpan duration { get; set; }
            public Guid executionId { get; set; }
        }

        public override Task<SiLAFramework.CommandConfirmation> Shake(Shake_Parameters request, ServerCallContext context)
        {
            const string commandIdentifier = "Shake";

            Log.Info($"\n\"{new StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            WorkerArgs workerArgs = new WorkerArgs();
            // check duration parameter
            try
            {
                workerArgs.duration = XmlConvert.ToTimeSpan(request.Duration.Value);
            }
            catch
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardValidationError("Duration", 
                    "Invalid duration notation", "Specify the duration in ISO 8601 notation"));
            }

            // check if clamp is closed
            if (_shakerSimulator.ClampState != ClampState.Closed)
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardExecutionError("ClampNotClosedError",
                    "Unable to shake with opened clamp",
                    "Close the clamp"));
            }

            // run shake command in background worker
            _worker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = false
            };

            // add observable command execution
            var executionId = _server.CommandExecutionManager.AddObservableCommandExecution(commandIdentifier, _executionTime);
            workerArgs.executionId = executionId;

            _worker.DoWork += worker_DoWork;
            _worker.ProgressChanged += worker_ProgressChanged;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.RunWorkerAsync(workerArgs);

            return Task.FromResult(new SiLAFramework.CommandConfirmation
            {
                CommandId = new SiLAFramework.CommandExecutionUUID
                {
                    CommandId = executionId.ToString()
                }
            });
        }

        public override Task<StopShaking_Responses> StopShaking(StopShaking_Parameters request, ServerCallContext context)
        {
            // todo implement
            return base.StopShaking(request, context);
        }

        public override async Task Shake_State(SiLAFramework.CommandExecutionUUID request, IServerStreamWriter<SiLAFramework.ExecutionInfo> responseStream, ServerCallContext context)
        {
            const string commandIdentifier = "Shake";

            Log.Info($"\n\"{new StackTrace().GetFrame(2).GetMethod().Name}\" request received");

            var commandId = Guid.Parse(request.CommandId);
            // check command ID
            _server.CommandExecutionManager.CheckCommandExecutionId(commandId, commandIdentifier);
            SiLAFramework.ExecutionInfo ExecutionInfo = _server.CommandExecutionManager.GetObservableCommandExecutionInfo(commandId);

            // check for progress changed
            while (ExecutionInfo.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.Waiting ||
                   ExecutionInfo.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.Running)
            {
                if (_progressChanged)
                {
                    await responseStream.WriteAsync(new SiLAFramework.ExecutionInfo { ProgressInfo = new SiLAFramework.Real { Value = _shakingProgressInPercent / 100.0 }, CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.Running });
                    _progressChanged = false;
                }

                await Task.Delay(500);
            }

            await responseStream.WriteAsync(new SiLAFramework.ExecutionInfo { ProgressInfo = new SiLAFramework.Real { Value = 1 }, CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully });
        }

        public override Task<Shake_Responses> Shake_Result(SiLAFramework.CommandExecutionUUID request, ServerCallContext context)
        {
            const string commandIdentifier = "Shake";

            Log.Info($"\n\"{new StackTrace().GetFrame(0).GetMethod().Name}\" request received");

            var commandId = Guid.Parse(request.CommandId);
            // check command ID
            _server.CommandExecutionManager.CheckCommandExecutionId(commandId, commandIdentifier);

            // check if command execution has been finished
            var commandStatus = _server.CommandExecutionManager.GetObservableCommandExecutionInfo(commandId).CommandStatus;
            
            // check if shaking has been finished
            if (commandStatus != SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully &&
                commandStatus != SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
            {
                ErrorHandling.raiseRPCError(ErrorHandling.GetStandardFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.CommandExecutionNotFinished));
            }

            return Task.FromResult(new Shake_Responses
            {
                ReturnValue = new SiLAFramework.String
                {
                    Value = "Shake command successfully finished"
                }
            });
        }

        #endregion

        #endregion

        #region Shake worker

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            WorkerArgs workerArgs = e.Argument as WorkerArgs;
            e.Result = workerArgs.executionId;
            double progressStep = workerArgs.duration.TotalMilliseconds / 100.0;

            SiLAFramework.ExecutionInfo ExecutionInfo = _server.CommandExecutionManager.GetObservableCommandExecutionInfo(workerArgs.executionId);
            ExecutionInfo.CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.Running;
            
            // simulate shaking
            _shakingProgressInPercent = 0;
            _shakerSimulator.IsShaking = true;

            Log.Debug($"Shaking for {workerArgs.duration.TotalSeconds} seconds started...");

            while (!((BackgroundWorker)sender).CancellationPending && _shakingProgressInPercent <= 100)
            {
                System.Threading.Thread.Sleep((int)progressStep);

                _shakingProgressInPercent++;
                ((BackgroundWorker)sender).ReportProgress(_shakingProgressInPercent);
            }

            Log.Info("\nShaking successfully finished");
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _progressChanged = true;
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Guid executionId = (Guid) e.Result;
            SiLAFramework.ExecutionInfo ExecutionInfo = _server.CommandExecutionManager.GetObservableCommandExecutionInfo(executionId);
            ExecutionInfo.CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully;
            _shakerSimulator.IsShaking = false;
        }

        #endregion
    }
}