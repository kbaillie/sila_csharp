﻿namespace SiLAClient
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using Common.Logging;
    using Grpc.Core;
    using SiLAFramework = Sila2.Org.Silastandard;
    using Shaking = Sila2.De.Equicon.Mixing.Shakingcontrol.V1;
    using Handling = Sila2.De.Equicon.Handling.Platehandlingcontrol.V1;
    using sila2;
    using Sila2.Org.Silastandard.Core.Silaservice.V1;

    public class ShakerClient
    {
        private readonly SiLAService.SiLAServiceClient _silaServiceClient;
        private readonly Shaking.ShakingControl.ShakingControlClient _shakingClient;
        private readonly Handling.PlateHandlingControl.PlateHandlingControlClient _handlingClient;
        private static ILog Log = LogManager.GetLogger<ShakerClient>();

        public ShakerClient(SiLAService.SiLAServiceClient silaServiceClient, Shaking.ShakingControl.ShakingControlClient shakingClient, Handling.PlateHandlingControl.PlateHandlingControlClient handlingClient)
        {
            this._silaServiceClient = silaServiceClient;
            this._shakingClient = shakingClient;
            this._handlingClient = handlingClient;
        }

        #region SiLAService commands

        public string GetDisplayName()
        {
            Get_ServerName_Responses response;
            try
            {
                response = _silaServiceClient.Get_ServerName(new Get_ServerName_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return string.Empty;
            }

            return response.ServerName.Value;
        }

        public string GetServerDescription()
        {
            Get_ServerDescription_Responses response;
            try
            {
                response = _silaServiceClient.Get_ServerDescription(new Get_ServerDescription_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return string.Empty;
            }

            return response.ServerDescription.Value;
        }

        public string GetServerVendorUrl()
        {
            Get_ServerVendorURL_Responses response;
            try
            {
                response = _silaServiceClient.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return string.Empty;
            }

            return response.ServerVendorURL.URL.Value;
        }

        public string GetServerVersion()
        {
            Get_ServerVersion_Responses response;
            try
            {
                response = _silaServiceClient.Get_ServerVersion(new Get_ServerVersion_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return string.Empty;
            }

            return response.ServerVersion.Value;
        }

        public List<string> GetImplementedFeatures()
        {
            Get_ImplementedFeatures_Responses response;
            try
            {
                response = _silaServiceClient.Get_ImplementedFeatures(new Get_ImplementedFeatures_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return new List<string>();
            }

            List<string> result = new List<string>();
            foreach (var featureQualifier in response.ImplementedFeatures)
            {
                result.Add(featureQualifier.FeatureIdentifier.Value);
            }

            return result;
        }

        public Feature GetFeatureDefinition(string featureIdentifier)
        {
            GetFeatureDefinition_Responses response;
            try
            {
                response = _silaServiceClient.GetFeatureDefinition(new GetFeatureDefinition_Parameters { QualifiedFeatureIdentifier = new DataType_FeatureIdentifier { FeatureIdentifier = new SiLAFramework.String { Value = featureIdentifier } } });
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                return null;
            }

            try
            {
                return FeatureGenerator.ReadFeatureFromXml(response.FeatureDefinition.FeatureDefinition.Value);
            }
            catch (Exception e)
            {
                Log.Error("GetFeatureDefinition failed: returned object is not a valid Feature definition, Details: " + e.Message);
            }
            return null;
        }

        #endregion

        #region ShakingControl commands

        public async Task Shake(string duration, int speed)
        {
            var cmdId = _shakingClient.Shake(new Shaking.Shake_Parameters { Duration = new SiLAFramework.String { Value = duration }, Speed = new SiLAFramework.Integer { Value = speed } }).CommandId;

            Console.WriteLine("Shake command started...");

            // wait for progress has been finished
            try
            {
                using (var call = _shakingClient.Shake_State(cmdId))
                {
                    var responseStream = call.ResponseStream;

                    while (await responseStream.MoveNext())
                    {
                        Console.Write("\rProgress: {0:0.00}     Status: {1}", responseStream.Current.ProgressInfo.Value, responseStream.Current.CommandStatus);
                        if (responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                    }
                    Console.Write("\n");
                }
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }

            // get result
            try
            {
                Shaking.Shake_Responses response = _shakingClient.Shake_Result(cmdId);
                Console.WriteLine("Shake command response received: " + response.ReturnValue.Value);
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }
        }

        #endregion

        #region Plate Handling commands

        public void OpenClamp()
        {
            Handling.OpenClamp_Responses response;
            try
            {
                response = _handlingClient.OpenClamp(new Handling.OpenClamp_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }
        }

        public void CloseClamp()
        {
            Handling.CloseClamp_Responses response;
            try
            {
                response = _handlingClient.CloseClamp(new Handling.CloseClamp_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }
        }

        public bool IsClampOpen()
        {
            Handling.Get_CurrentClampState_Responses response;
            try
            {
                response = _handlingClient.Get_CurrentClampState(new Handling.Get_CurrentClampState_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }

            return response.CurrentClampState.Value;
        }

        #endregion

        #region Helper methods

        public static object StringToObject(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                return new BinaryFormatter().Deserialize(ms);
            }
        }
        #endregion
    }
}