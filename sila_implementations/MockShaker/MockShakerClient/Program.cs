﻿namespace SiLAClient
{
    using System;
    using Grpc.Core;
    using sila2;
    using sila2.Discovery;

    class Program
    {
        private static void DisplayClampState(ShakerClient client)
        {
            Console.WriteLine("Clamp state = " + (client.IsClampOpen() ? "open" : "closed"));
        }

        static void Main(string[] args)
        {
            try
            {
                Channel channel;
                try
                {
                    // use SiLA Server Discovery to find the mock shaker server
                    const string serviceType = "_sila._tcp";
                    string namePattern = "Mock Shaker";
                    Console.WriteLine("\nServer Discovery:\nQuerying for service type \"{0}\" with name pattern \"{1}\"...", serviceType, namePattern);
                    var foundServices = ServiceFinder.QueryAllServicesByName("_sila._tcp", namePattern, 2000);
                    if (foundServices.Count > 0)
                    {
                        Console.WriteLine("{0} service{1} found:", foundServices.Count, foundServices.Count == 1 ? string.Empty : "s");
                        foreach (var service in foundServices)
                        {
                            Console.WriteLine(" - \"{0}\"(host: {1}) at {2}", service.ServiceName, service.HostName, service.Address);
                        }

                        // create channel
                        Console.WriteLine("\nConnecting to service \"{0}\" on {1} ...", foundServices[0].ServiceName, foundServices[0].Address);
                        channel = new Channel(foundServices[0].Address, ChannelCredentials.Insecure);
                    }
                    else
                    {
                        // in case SiLA Service Discovery didn't work, use command line service port (default: 50052)
                        int port = args != null && args.Length > 0 ? int.Parse(args[0]) : 50052;
                        channel = new Channel("127.0.0.1:" + port, ChannelCredentials.Insecure);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception while trying to connect to the server: " + e.Message);
                    return;
                }

                // create the client
                var client = new ShakerClient(new Sila2.Org.Silastandard.Core.Silaservice.V1.SiLAService.SiLAServiceClient(channel),
                    new Sila2.De.Equicon.Mixing.Shakingcontrol.V1.ShakingControl.ShakingControlClient(channel),
                    new Sila2.De.Equicon.Handling.Platehandlingcontrol.V1.PlateHandlingControl.PlateHandlingControlClient(channel));

                // read properties from SiLAService feature
                Console.WriteLine("Server display name: " + client.GetDisplayName());
                Console.WriteLine("Server description: " + client.GetServerDescription());
                Console.WriteLine("Server vendor URL: " + client.GetServerVendorUrl());
                Console.WriteLine("Server version: " + client.GetServerVersion());

                // feature discovery
                Console.WriteLine("\nFeature Discovery:");

                // get implemented features
                var list = client.GetImplementedFeatures();
                Console.WriteLine("{0} implemented Feature{1} found:", list.Count, list.Count == 1 ? string.Empty : "s");
                foreach (var f in list)
                {
                    Console.WriteLine(" - " + f);
                }

                // get feature definitions
                foreach (var featureQualifier in list)
                {
                    Console.WriteLine("\n-------------------------------------------------------------------------");
                    Console.WriteLine(" Feature Definition of " + featureQualifier);
                    Console.WriteLine("-------------------------------------------------------------------------");
                    Console.WriteLine(client.GetFeatureDefinition(featureQualifier).ToString());
                }

                Console.WriteLine(string.Empty);

                // try getting a feature definition with invalid feature identifier
                try
                {
                    Console.WriteLine("Calling \"GetFeatureDefinition(\"something\")\" ...");
                    client.GetFeatureDefinition("something");
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // test clamp handling
                DisplayClampState(client);
                Console.WriteLine("Calling \"OpenClamp()\" ...");
                client.OpenClamp();
                DisplayClampState(client);

                // try shaking with invalid duration parameter
                try
                {
                    Console.WriteLine("Calling \"Shake(\"7S\", 3000)\" ...");
                    client.Shake("7S", 3000).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // try shaking with opened clamp
                try
                {
                    Console.WriteLine("Calling \"Shake(\"PT7S\", 3000)\" ...");
                    client.Shake("PT7S", 3000).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // close clamp in order to shake
                Console.WriteLine("Calling \"CloseClamp()\" ...");
                client.CloseClamp();
                DisplayClampState(client);

                // shake for 7 seconds
                Console.WriteLine("Calling \"Shake(\"7S\", 3000)\" ...");
                client.Shake("PT7S", 3000).Wait();

                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                Console.WriteLine(ErrorHandling.HandleException(e));
            }

            Console.ReadKey();
        }
    }
}