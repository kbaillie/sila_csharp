// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ShakingControl.proto
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Sila2.De.Equicon.Mixing.Shakingcontrol.V1 {

  /// <summary>Holder for reflection information generated from ShakingControl.proto</summary>
  public static partial class ShakingControlReflection {

    #region Descriptor
    /// <summary>File descriptor for ShakingControl.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static ShakingControlReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "ChRTaGFraW5nQ29udHJvbC5wcm90bxIpc2lsYTIuZGUuZXF1aWNvbi5taXhp",
            "bmcuc2hha2luZ2NvbnRyb2wudjEaE1NpTEFGcmFtZXdvcmsucHJvdG8idAoQ",
            "U2hha2VfUGFyYW1ldGVycxIwCghEdXJhdGlvbhgBIAEoCzIeLnNpbGEyLm9y",
            "Zy5zaWxhc3RhbmRhcmQuU3RyaW5nEi4KBVNwZWVkGAIgASgLMh8uc2lsYTIu",
            "b3JnLnNpbGFzdGFuZGFyZC5JbnRlZ2VyIkYKD1NoYWtlX1Jlc3BvbnNlcxIz",
            "CgtSZXR1cm5WYWx1ZRgBIAEoCzIeLnNpbGEyLm9yZy5zaWxhc3RhbmRhcmQu",
            "U3RyaW5nIhgKFlN0b3BTaGFraW5nX1BhcmFtZXRlcnMiTAoVU3RvcFNoYWtp",
            "bmdfUmVzcG9uc2VzEjMKC1JldHVyblZhbHVlGAEgASgLMh4uc2lsYTIub3Jn",
            "LnNpbGFzdGFuZGFyZC5TdHJpbmcygAQKDlNoYWtpbmdDb250cm9sEnMKBVNo",
            "YWtlEjsuc2lsYTIuZGUuZXF1aWNvbi5taXhpbmcuc2hha2luZ2NvbnRyb2wu",
            "djEuU2hha2VfUGFyYW1ldGVycxorLnNpbGEyLm9yZy5zaWxhc3RhbmRhcmQu",
            "Q29tbWFuZENvbmZpcm1hdGlvbiIAEmYKC1NoYWtlX1N0YXRlEiwuc2lsYTIu",
            "b3JnLnNpbGFzdGFuZGFyZC5Db21tYW5kRXhlY3V0aW9uVVVJRBolLnNpbGEy",
            "Lm9yZy5zaWxhc3RhbmRhcmQuRXhlY3V0aW9uSW5mbyIAMAESegoMU2hha2Vf",
            "UmVzdWx0Eiwuc2lsYTIub3JnLnNpbGFzdGFuZGFyZC5Db21tYW5kRXhlY3V0",
            "aW9uVVVJRBo6LnNpbGEyLmRlLmVxdWljb24ubWl4aW5nLnNoYWtpbmdjb250",
            "cm9sLnYxLlNoYWtlX1Jlc3BvbnNlcyIAEpQBCgtTdG9wU2hha2luZxJBLnNp",
            "bGEyLmRlLmVxdWljb24ubWl4aW5nLnNoYWtpbmdjb250cm9sLnYxLlN0b3BT",
            "aGFraW5nX1BhcmFtZXRlcnMaQC5zaWxhMi5kZS5lcXVpY29uLm1peGluZy5z",
            "aGFraW5nY29udHJvbC52MS5TdG9wU2hha2luZ19SZXNwb25zZXMiAGIGcHJv",
            "dG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Sila2.Org.Silastandard.SiLAFrameworkReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.Shake_Parameters), global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.Shake_Parameters.Parser, new[]{ "Duration", "Speed" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.Shake_Responses), global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.Shake_Responses.Parser, new[]{ "ReturnValue" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.StopShaking_Parameters), global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.StopShaking_Parameters.Parser, null, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.StopShaking_Responses), global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.StopShaking_Responses.Parser, new[]{ "ReturnValue" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class Shake_Parameters : pb::IMessage<Shake_Parameters> {
    private static readonly pb::MessageParser<Shake_Parameters> _parser = new pb::MessageParser<Shake_Parameters>(() => new Shake_Parameters());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Shake_Parameters> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.ShakingControlReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Shake_Parameters() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Shake_Parameters(Shake_Parameters other) : this() {
      Duration = other.duration_ != null ? other.Duration.Clone() : null;
      Speed = other.speed_ != null ? other.Speed.Clone() : null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Shake_Parameters Clone() {
      return new Shake_Parameters(this);
    }

    /// <summary>Field number for the "Duration" field.</summary>
    public const int DurationFieldNumber = 1;
    private global::Sila2.Org.Silastandard.String duration_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Sila2.Org.Silastandard.String Duration {
      get { return duration_; }
      set {
        duration_ = value;
      }
    }

    /// <summary>Field number for the "Speed" field.</summary>
    public const int SpeedFieldNumber = 2;
    private global::Sila2.Org.Silastandard.Integer speed_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Sila2.Org.Silastandard.Integer Speed {
      get { return speed_; }
      set {
        speed_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Shake_Parameters);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Shake_Parameters other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(Duration, other.Duration)) return false;
      if (!object.Equals(Speed, other.Speed)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (duration_ != null) hash ^= Duration.GetHashCode();
      if (speed_ != null) hash ^= Speed.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (duration_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(Duration);
      }
      if (speed_ != null) {
        output.WriteRawTag(18);
        output.WriteMessage(Speed);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (duration_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(Duration);
      }
      if (speed_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(Speed);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Shake_Parameters other) {
      if (other == null) {
        return;
      }
      if (other.duration_ != null) {
        if (duration_ == null) {
          duration_ = new global::Sila2.Org.Silastandard.String();
        }
        Duration.MergeFrom(other.Duration);
      }
      if (other.speed_ != null) {
        if (speed_ == null) {
          speed_ = new global::Sila2.Org.Silastandard.Integer();
        }
        Speed.MergeFrom(other.Speed);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            if (duration_ == null) {
              duration_ = new global::Sila2.Org.Silastandard.String();
            }
            input.ReadMessage(duration_);
            break;
          }
          case 18: {
            if (speed_ == null) {
              speed_ = new global::Sila2.Org.Silastandard.Integer();
            }
            input.ReadMessage(speed_);
            break;
          }
        }
      }
    }

  }

  public sealed partial class Shake_Responses : pb::IMessage<Shake_Responses> {
    private static readonly pb::MessageParser<Shake_Responses> _parser = new pb::MessageParser<Shake_Responses>(() => new Shake_Responses());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Shake_Responses> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.ShakingControlReflection.Descriptor.MessageTypes[1]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Shake_Responses() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Shake_Responses(Shake_Responses other) : this() {
      ReturnValue = other.returnValue_ != null ? other.ReturnValue.Clone() : null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Shake_Responses Clone() {
      return new Shake_Responses(this);
    }

    /// <summary>Field number for the "ReturnValue" field.</summary>
    public const int ReturnValueFieldNumber = 1;
    private global::Sila2.Org.Silastandard.String returnValue_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Sila2.Org.Silastandard.String ReturnValue {
      get { return returnValue_; }
      set {
        returnValue_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Shake_Responses);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Shake_Responses other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(ReturnValue, other.ReturnValue)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (returnValue_ != null) hash ^= ReturnValue.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (returnValue_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(ReturnValue);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (returnValue_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(ReturnValue);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Shake_Responses other) {
      if (other == null) {
        return;
      }
      if (other.returnValue_ != null) {
        if (returnValue_ == null) {
          returnValue_ = new global::Sila2.Org.Silastandard.String();
        }
        ReturnValue.MergeFrom(other.ReturnValue);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            if (returnValue_ == null) {
              returnValue_ = new global::Sila2.Org.Silastandard.String();
            }
            input.ReadMessage(returnValue_);
            break;
          }
        }
      }
    }

  }

  public sealed partial class StopShaking_Parameters : pb::IMessage<StopShaking_Parameters> {
    private static readonly pb::MessageParser<StopShaking_Parameters> _parser = new pb::MessageParser<StopShaking_Parameters>(() => new StopShaking_Parameters());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<StopShaking_Parameters> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.ShakingControlReflection.Descriptor.MessageTypes[2]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StopShaking_Parameters() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StopShaking_Parameters(StopShaking_Parameters other) : this() {
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StopShaking_Parameters Clone() {
      return new StopShaking_Parameters(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as StopShaking_Parameters);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(StopShaking_Parameters other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(StopShaking_Parameters other) {
      if (other == null) {
        return;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
        }
      }
    }

  }

  public sealed partial class StopShaking_Responses : pb::IMessage<StopShaking_Responses> {
    private static readonly pb::MessageParser<StopShaking_Responses> _parser = new pb::MessageParser<StopShaking_Responses>(() => new StopShaking_Responses());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<StopShaking_Responses> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Sila2.De.Equicon.Mixing.Shakingcontrol.V1.ShakingControlReflection.Descriptor.MessageTypes[3]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StopShaking_Responses() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StopShaking_Responses(StopShaking_Responses other) : this() {
      ReturnValue = other.returnValue_ != null ? other.ReturnValue.Clone() : null;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public StopShaking_Responses Clone() {
      return new StopShaking_Responses(this);
    }

    /// <summary>Field number for the "ReturnValue" field.</summary>
    public const int ReturnValueFieldNumber = 1;
    private global::Sila2.Org.Silastandard.String returnValue_;
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public global::Sila2.Org.Silastandard.String ReturnValue {
      get { return returnValue_; }
      set {
        returnValue_ = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as StopShaking_Responses);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(StopShaking_Responses other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if (!object.Equals(ReturnValue, other.ReturnValue)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      if (returnValue_ != null) hash ^= ReturnValue.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      if (returnValue_ != null) {
        output.WriteRawTag(10);
        output.WriteMessage(ReturnValue);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      if (returnValue_ != null) {
        size += 1 + pb::CodedOutputStream.ComputeMessageSize(ReturnValue);
      }
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(StopShaking_Responses other) {
      if (other == null) {
        return;
      }
      if (other.returnValue_ != null) {
        if (returnValue_ == null) {
          returnValue_ = new global::Sila2.Org.Silastandard.String();
        }
        ReturnValue.MergeFrom(other.ReturnValue);
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            if (returnValue_ == null) {
              returnValue_ = new global::Sila2.Org.Silastandard.String();
            }
            input.ReadMessage(returnValue_);
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
