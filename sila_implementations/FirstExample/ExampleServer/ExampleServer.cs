﻿namespace ExampleServer
{
    using System;
    using System.Threading;
    using Common.Logging;
    using sila2;
    using sila2.Utils;
    using CommandLine;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class ExampleServer : SiLA2Server
    {
        private readonly GreetingProviderImpl _greetingProviderImpl;
        private readonly TemperatureControllerImpl _temperatureControllerImpl;
        public Boolean GreetingHappened => _greetingProviderImpl.GreetingHappened;
        private static ILog Log = LogManager.GetLogger<ExampleServer>();
        public ExampleServer(int portNumber, string interfaceNameOrCIDR, string configFile)
        : base(new ServerInformation(
                "Example Server",
                "Example Server that implements the GreetingsProvider and the TemperatureController feature",
                "www.equicon.de",
                "1.0"),
                portNumber,
                interfaceNameOrCIDR,
                configFile)
        {
            // add GreetingProvider feature implementation
            this.ReadFeature("features/GreetingProvider.xml");
            _greetingProviderImpl = new GreetingProviderImpl();
            this.GrpcServer.Services.Add(Sila2.Org.Silastandard.Examples.Greetingprovider.V1.GreetingProvider.BindService(_greetingProviderImpl));

            // add TemperatureController feature implementation
            var temperatureControllerFeature = this.ReadFeature("features/TemperatureController.xml");
            _temperatureControllerImpl = new TemperatureControllerImpl { GreetingProviderImpl = _greetingProviderImpl, SiLAServer = this, SiLAFeature = temperatureControllerFeature };
            this.GrpcServer.Services.Add(Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureController.BindService(_temperatureControllerImpl));

            // set current year as server start year
            _greetingProviderImpl.ServerStartYear = DateTime.Now.Year;

            this.CommandExecutionManager.OnCommandRemoved += (id, cmd) =>
            {
                Console.WriteLine($"Command {cmd.CommandIdentifier} execution with id " + id + " has been removed");
            };
        }

        public void Stop()
        {
            this.ShutdownServer();
        }

        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = LogManager.GetLogger<ExampleServer>();
            try
            {
                ServerCmdLineArgs options = null;
                Parser.Default.ParseArguments<ServerCmdLineArgs>(args)
                   .WithParsed<ServerCmdLineArgs>(o => options = o)
                   .WithNotParsed<ServerCmdLineArgs>(errs => ServerCmdLineArgs.HandleParseError(errs));

                // Server Configuration file
                // create SiLA2 Server
                ExampleServer server = new ExampleServer(options.Port, options.InterfaceNameOrCIDR, options.ConfigFile);
                server.StartServer();
                Log.Info("Example Server listening on port " + options.Port);

                Thread.Sleep(3000);
                Console.WriteLine("\nPress enter to stop the server...");
                Console.ReadLine();

                server.ShutdownServer();
            }
            catch (Exception e)
            {
                Log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}
