﻿namespace ExampleServer
{
    using System.Threading.Tasks;
    using Grpc.Core;
    using Sila2.Org.Silastandard.Examples.Greetingprovider.V1;
    using SiLAFramework = Sila2.Org.Silastandard;

    /// <summary>
    /// Implements the functionality of the GreetingsProviderImpl feature.
    /// </summary>
    internal class GreetingProviderImpl : GreetingProvider.GreetingProviderBase
    {
        /// <summary>Gets a value indicating whether the SayHello command has been called at least one time before.</summary>
        public bool GreetingHappened { get; private set; }

        /// <summary>Gets or sets the start year of the server</summary>
        public int ServerStartYear { private get; set; }

        #region Overrides of GreetingProviderBase

        /// <summary>Answers the SayHello RPC by retunring a greeting message.</summary>
        /// <param name="request">The call parameters containing the Name parameter.</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns>The call response containing the Greeting field.</returns>
        public override Task<SayHello_Responses> SayHello(SayHello_Parameters request, ServerCallContext context)
        {
            if (!this.GreetingHappened) { this.GreetingHappened = true; }

            return Task.FromResult(new SayHello_Responses { Greeting = new SiLAFramework.String { Value = "Hello " + request.Name.Value } });
        }

        /// <summary>
        /// Receives the unobservable property RPC and returns the current year as a field of the call response.
        /// </summary>
        /// <param name="request">The call parameters (empty for properties).</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns>The call response containing the CurrentYear field.</returns>
        public override Task<Get_StartYear_Responses> Get_StartYear(Get_StartYear_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StartYear_Responses { StartYear = new SiLAFramework.Integer { Value = ServerStartYear } });
        }

        #endregion
    }
}
