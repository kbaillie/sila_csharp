// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: TemperatureController.proto
#pragma warning disable 1591
#region Designer generated code

using System;
using System.Threading;
using System.Threading.Tasks;
using grpc = global::Grpc.Core;

namespace Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1 {
  public static partial class TemperatureController
  {
    static readonly string __ServiceName = "sila2.org.silastandard.examples.temperaturecontroller.v1.TemperatureController";

    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters> __Marshaller_ControlTemperature_Parameters = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.CommandConfirmation> __Marshaller_CommandConfirmation = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.CommandConfirmation.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.CommandExecutionUUID> __Marshaller_CommandExecutionUUID = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.CommandExecutionUUID.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.ExecutionInfo> __Marshaller_ExecutionInfo = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.ExecutionInfo.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses> __Marshaller_ControlTemperature_Responses = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters> __Marshaller_Subscribe_CurrentTemperature_Parameters = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses> __Marshaller_Subscribe_CurrentTemperature_Responses = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses.Parser.ParseFrom);

    static readonly grpc::Method<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters, global::Sila2.Org.Silastandard.CommandConfirmation> __Method_ControlTemperature = new grpc::Method<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters, global::Sila2.Org.Silastandard.CommandConfirmation>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ControlTemperature",
        __Marshaller_ControlTemperature_Parameters,
        __Marshaller_CommandConfirmation);

    static readonly grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.ExecutionInfo> __Method_ControlTemperature_State = new grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.ExecutionInfo>(
        grpc::MethodType.ServerStreaming,
        __ServiceName,
        "ControlTemperature_State",
        __Marshaller_CommandExecutionUUID,
        __Marshaller_ExecutionInfo);

    static readonly grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses> __Method_ControlTemperature_Result = new grpc::Method<global::Sila2.Org.Silastandard.CommandExecutionUUID, global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses>(
        grpc::MethodType.Unary,
        __ServiceName,
        "ControlTemperature_Result",
        __Marshaller_CommandExecutionUUID,
        __Marshaller_ControlTemperature_Responses);

    static readonly grpc::Method<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters, global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses> __Method_Subscribe_CurrentTemperature = new grpc::Method<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters, global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses>(
        grpc::MethodType.ServerStreaming,
        __ServiceName,
        "Subscribe_CurrentTemperature",
        __Marshaller_Subscribe_CurrentTemperature_Parameters,
        __Marshaller_Subscribe_CurrentTemperature_Responses);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.TemperatureControllerReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of TemperatureController</summary>
    public abstract partial class TemperatureControllerBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Sila2.Org.Silastandard.CommandConfirmation> ControlTemperature(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task ControlTemperature_State(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::IServerStreamWriter<global::Sila2.Org.Silastandard.ExecutionInfo> responseStream, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses> ControlTemperature_Result(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

      public virtual global::System.Threading.Tasks.Task Subscribe_CurrentTemperature(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters request, grpc::IServerStreamWriter<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses> responseStream, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for TemperatureController</summary>
    public partial class TemperatureControllerClient : grpc::ClientBase<TemperatureControllerClient>
    {
      /// <summary>Creates a new client for TemperatureController</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public TemperatureControllerClient(grpc::Channel channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for TemperatureController that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public TemperatureControllerClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected TemperatureControllerClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected TemperatureControllerClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Sila2.Org.Silastandard.CommandConfirmation ControlTemperature(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters request, grpc::Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return ControlTemperature(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Sila2.Org.Silastandard.CommandConfirmation ControlTemperature(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ControlTemperature, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.CommandConfirmation> ControlTemperatureAsync(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters request, grpc::Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return ControlTemperatureAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.CommandConfirmation> ControlTemperatureAsync(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ControlTemperature, null, options, request);
      }
      public virtual grpc::AsyncServerStreamingCall<global::Sila2.Org.Silastandard.ExecutionInfo> ControlTemperature_State(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return ControlTemperature_State(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncServerStreamingCall<global::Sila2.Org.Silastandard.ExecutionInfo> ControlTemperature_State(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncServerStreamingCall(__Method_ControlTemperature_State, null, options, request);
      }
      public virtual global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses ControlTemperature_Result(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return ControlTemperature_Result(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses ControlTemperature_Result(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_ControlTemperature_Result, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses> ControlTemperature_ResultAsync(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return ControlTemperature_ResultAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.ControlTemperature_Responses> ControlTemperature_ResultAsync(global::Sila2.Org.Silastandard.CommandExecutionUUID request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_ControlTemperature_Result, null, options, request);
      }
      public virtual grpc::AsyncServerStreamingCall<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses> Subscribe_CurrentTemperature(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters request, grpc::Metadata headers = null, DateTime? deadline = null, CancellationToken cancellationToken = default(CancellationToken))
      {
        return Subscribe_CurrentTemperature(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncServerStreamingCall<global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Responses> Subscribe_CurrentTemperature(global::Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1.Subscribe_CurrentTemperature_Parameters request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncServerStreamingCall(__Method_Subscribe_CurrentTemperature, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override TemperatureControllerClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new TemperatureControllerClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(TemperatureControllerBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_ControlTemperature, serviceImpl.ControlTemperature)
          .AddMethod(__Method_ControlTemperature_State, serviceImpl.ControlTemperature_State)
          .AddMethod(__Method_ControlTemperature_Result, serviceImpl.ControlTemperature_Result)
          .AddMethod(__Method_Subscribe_CurrentTemperature, serviceImpl.Subscribe_CurrentTemperature).Build();
    }

  }
}
#endregion
