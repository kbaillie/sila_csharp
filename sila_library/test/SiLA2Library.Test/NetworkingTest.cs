using System;
using Xunit;
using sila2;
using Moq;
using System.Net.NetworkInformation;
using System.Collections.Generic;

namespace SiLA2Library.Test
{
	public class NetworkingTest
	{
		#region Helper Functions

		//Class for access protected members for testing
		//If the depenency injected functions are exposed publicly then this can be removed
		public class NetworkingMock : Networking
		{
			public new static NetworkInterface GetNetworkInterfaceByCIDR(string mask, NetworkInterfaceProvider networkInterfaceProvider)
			{
				return Networking.GetNetworkInterfaceByCIDR(mask, networkInterfaceProvider);
			}

			public new static NetworkInterface GetNetworkInterfaceByName(string name, NetworkInterfaceProvider networkInterfaceProvider)
			{
				return Networking.GetNetworkInterfaceByName(name, networkInterfaceProvider);
			}
		}

		Mock<NetworkInterfaceProvider> GetNetworkInterfaceProviderMock(List<NetworkInterface> networkInterfaces)
		{
			//networkInterface.
			Moq.Mock<NetworkInterfaceProvider> providerMock = new Mock<NetworkInterfaceProvider>();
			providerMock.Setup(x => x.GetAllNetworkInterfaces()).Returns(networkInterfaces.ToArray());
			return providerMock;
		}

		Mock<NetworkInterface> GetNetworkInterfaceMock(string name, string[] ips, OperationalStatus status)
		{
			//Create objects for any IPs
			List<UnicastIPAddressInformation> addressList = new List<UnicastIPAddressInformation>();
			foreach(string ipAddress in ips)
			{
				System.Net.IPAddress address = System.Net.IPAddress.Parse(ipAddress);
				Mock<UnicastIPAddressInformation> addrMock = new Mock<UnicastIPAddressInformation>();
				addrMock.SetupGet(x => x.Address).Returns(address);

				addressList.Add(addrMock.Object);
			}
			
			//Mock the essentials of the read only collection
			Mock<UnicastIPAddressInformationCollection> collectionMock = new Mock<UnicastIPAddressInformationCollection>();
			collectionMock.Setup(x => x.GetEnumerator()).Returns(addressList.GetEnumerator());
			collectionMock.SetupGet(x => x.Count).Returns(addressList.Count);
			collectionMock.Setup(x => x[It.IsAny<int>()]).Returns((int a) => addressList[a]);
			collectionMock.Setup(x => x.Contains(It.IsAny<UnicastIPAddressInformation>())).Returns((UnicastIPAddressInformation a) => addressList.Contains(a));

			//Mock the unicast addresses property
			//If we're concerned about more than just unicast addresses, other parts of this will have to be mocked
			Mock<IPInterfaceProperties> iPMock = new Mock<IPInterfaceProperties>();
			iPMock.SetupGet(x => x.UnicastAddresses).Returns(collectionMock.Object);

			//Create the NetworkInterface mock itself
			Mock<NetworkInterface> networkMock = new Mock<NetworkInterface>();
			networkMock.SetupGet(x => x.OperationalStatus).Returns(status);
			networkMock.SetupGet(x => x.Name).Returns(name);
			networkMock.Setup(x => x.GetIPProperties()).Returns(iPMock.Object);

			return networkMock;
		}

		#endregion

		[Fact]
		public void ShouldThrowNetworkInterfaceNotFoundExceptionWhenInterfaceNameNull()
		{
			Assert.Throws<NetworkInterfaceNotFoundException>(() => Networking.GetNetworkInterfaceByName(null));
		}

		[Fact]
		public void ShouldFindANetworkInterfaceByIP()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.1" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			NetworkInterface networkInterface = NetworkingMock.GetNetworkInterfaceByCIDR("192.168.1.1", providerMock.Object);
			Assert.Equal("Adapter1", networkInterface.Name);
		}

		[Fact]
		public void ShouldNotFindNetworkInterfaceByIP()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.2" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "192.168.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "192.168.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByCIDR("192.168.1.1", providerMock.Object));
		}

		[Fact]
		public void ShouldFindNetworkInterfaceByCIDR()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.2" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "172.16.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "10.5.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			NetworkInterface foundInterface = NetworkingMock.GetNetworkInterfaceByCIDR("192.168.0.0/16", providerMock.Object);
			Assert.Equal(networkInterfaces[0], foundInterface);
			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByCIDR("192.168.0.0/16", providerMock.Object));
		}

		[Fact]
		public void ShouldNotFindNetworkInterfaceByCIDR()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "172.16.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "10.5.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByCIDR("192.168.0.0/16", providerMock.Object));
		}

		[Fact]
		public void ShouldNotFindNetworkInterfaceWithDownStatusByCIDR()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.2" }, OperationalStatus.Down).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "172.16.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "10.5.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByCIDR("192.168.0.0/16", providerMock.Object));
		}

		[Fact]
		public void ShouldNotFindNetworkInterfaceWithDownStatusByIP()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.1" }, OperationalStatus.Down).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "192.168.1.2" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "192.168.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter4", new string[] { "192.168.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByCIDR("192.168.1.1", providerMock.Object));
		}

		[Fact]
		public void ShouldNotFindNetworkInterfaceWithDownStatusByName()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.1" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "192.168.1.2" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "192.168.1.3" }, OperationalStatus.Down).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter4", new string[] { "192.168.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByName("Adapter3", providerMock.Object));
		}

		[Fact]
		public void ShouldFindNetworkInterfaceByName()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.1" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "192.168.1.2" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "192.168.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter4", new string[] { "192.168.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			NetworkInterface foundInterface = NetworkingMock.GetNetworkInterfaceByName("Adapter3", providerMock.Object);
			Assert.Equal(networkInterfaces[2], foundInterface);
		}

		[Fact]
		public void ShouldNotFindNetworkInterfaceByName()
		{
			List<NetworkInterface> networkInterfaces = new List<NetworkInterface>();
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter1", new string[] { "192.168.1.1" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter2", new string[] { "192.168.1.2" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter3", new string[] { "192.168.1.3" }, OperationalStatus.Up).Object);
			networkInterfaces.Add(GetNetworkInterfaceMock("Adapter4", new string[] { "192.168.1.4" }, OperationalStatus.Up).Object);

			Mock<NetworkInterfaceProvider> providerMock = GetNetworkInterfaceProviderMock(networkInterfaces);

			Assert.Throws<NetworkInterfaceNotFoundException>(() => NetworkingMock.GetNetworkInterfaceByName("Adapter5", providerMock.Object));
		}
	}
}
