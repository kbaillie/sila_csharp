﻿namespace sila2
{
    using SiLAFramework = Sila2.Org.Silastandard;
    using System;

    public class ObservableCommandExecution
    {
        /// <summary>
        /// SiLA Command Identifier
        /// </summary>
        public string CommandIdentifier { get; }

        /// <summary>
        /// The current state of the command execution
        /// </summary>
        public SiLAFramework.ExecutionInfo CurrentState { get; }
        /// <summary>
        /// The minimum amount of time the command must be alive
        /// </summary>
        public DateTime MinCommandLifeTime { get; }

        /// <summary>
        /// The maximum amount of time the command can be alive
        /// </summary>
        public DateTime MaxCommandLifeTime { get; }

        /// <summary>
        /// Create an observable command execution
        /// </summary>
        /// <param name="commandIdentifier">Command identifier</param>
        /// <param name="commandLifeTime">
        /// Minimum amount of time in seconds the command will be available.
        /// Using a negative value will make the command
        /// available for the lifetime of the server.
        /// </param>
        /// <param name="deletionTimeDelay">
        /// The delay in seconds needed for the deletion of the command.
        /// The command will be removed after commandLifeTime + deletionTimeDelay seconds have elapsed.
        /// </param>
        public ObservableCommandExecution(
            string commandIdentifier,
            long commandLifeTime,
            ulong deletionTimeDelay)
        {
            if (string.IsNullOrEmpty(commandIdentifier))
            {
                throw new ArgumentException("Command identifier cannot be empty!");
            }

            MinCommandLifeTime = commandLifeTime < 0 ? DateTime.MaxValue : DateTime.Now.AddSeconds(commandLifeTime);
            MaxCommandLifeTime = commandLifeTime < 0 ? DateTime.MaxValue : MinCommandLifeTime.AddSeconds(deletionTimeDelay);
            CommandIdentifier = commandIdentifier;
            CurrentState = new SiLAFramework.ExecutionInfo
            {
                CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.Waiting,
                ProgressInfo = new SiLAFramework.Real { Value = 0.0 },
                EstimatedRemainingTime = new SiLAFramework.Duration { Nanos = 0, Seconds = 0 }
            };
        }
    }
}
