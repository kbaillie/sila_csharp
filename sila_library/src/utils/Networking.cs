namespace sila2
{
    using System;
    using System.Net;
    using System.Net.NetworkInformation;
    using Common.Logging;
    using System.Linq;
    using System.Collections;

    /// <summary>
    /// Networking utility library listing network interfaces and finding 
    /// interfaces based on name or CIDR
    /// </summary>
    public class Networking
    {
        private static ILog Log = LogManager.GetLogger<Networking>();

        /// <summary>
        /// Prints the list of network interfaces available on the system
        /// </summary>
        public static void PrintNetworkInterfaces()
        {
            var interfaces = NetworkInterface.GetAllNetworkInterfaces();
            Log.Info("Available Network Interfaces:");
            foreach (var item in interfaces)
            {
                Log.Info($"{item.Name}");
                Log.Info($"     {item.Id}");
                Log.Info($"     {item.NetworkInterfaceType}");
                Log.Info($"     {item.GetPhysicalAddress()}");
                Log.Info($"     {item.OperationalStatus}");
                var addresses = "";
                foreach (var ip in item.GetIPProperties().UnicastAddresses)
                {
                    addresses += $"{ip.Address} , ";
                }
                Log.Info($"     {addresses}");
            }
        }

        /// <summary>
        /// Finds the network interface that has a matching interface name or matching unicast address
        /// </summary>
        /// <param name="interfaceNameOrCIDR">interface name to find or the IPv4/IPv6 address or CIDR mask. </param>
        /// <returns>the matching found network interface</returns>
        public static NetworkInterface GetNetworkInterface(string interfaceNameOrCIDR)
        {    
            try
            {
                return Networking.GetNetworkInterfaceByCIDR(interfaceNameOrCIDR);
            }
            catch(NetworkInterfaceNotFoundException e)
            {
                Log.Warn($"Could not get network interface by IP address or CIDR: {e.Message}");
            }
            catch(FormatException e)
            {
                Log.Warn($"{interfaceNameOrCIDR} is not an IP address or CIDR mask");
            }

            try
            {
                return Networking.GetNetworkInterfaceByName(interfaceNameOrCIDR);
            }
            catch(NetworkInterfaceNotFoundException e)
            {
                Log.Warn($"Could not get network interface by name: {e.Message}");
            }
            return null;
        }

		public static NetworkInterface GetNetworkInterfaceByName(string interfaceName)
		{
			return GetNetworkInterfaceByName(interfaceName, new NetworkInterfaceProvider());
		}
		protected static NetworkInterface GetNetworkInterfaceByName(string interfaceName, NetworkInterfaceProvider networkInterfaceProvider)
		{
            if(interfaceName == null)
            {
                throw new NetworkInterfaceNotFoundException("No network interface name was specified");
            }

            foreach (var nic in networkInterfaceProvider.GetAllNetworkInterfaces())
            {
                if(interfaceName.Contains(nic.Name))
                {
                    if(nic.OperationalStatus != OperationalStatus.Up)
                    {
                        throw new NetworkInterfaceNotFoundException($"Interface {nic.Name} is not Up!");
                    }
                    return nic;
                }
            }
            throw new NetworkInterfaceNotFoundException($"Intereface {interfaceName} not found for Server Discovery");
        }

		/// <summary>
		/// Finds the first network interface that has a matching unicast address-
		/// </summary>
		/// <param name="mask">IPv4/IPv6 address or CIDR mask.</param>
		/// <returns>a matching interface</returns>
		public static NetworkInterface GetNetworkInterfaceByCIDR(string mask)
		{
			return GetNetworkInterfaceByCIDR(mask, new NetworkInterfaceProvider());
		}

		protected static NetworkInterface GetNetworkInterfaceByCIDR(string mask, NetworkInterfaceProvider networkInterfaceProvider)
		{
            if(mask.Contains('/'))
            {
                IPNetwork ipnetwork = IPNetwork.Parse(mask);
                foreach (NetworkInterface nic in networkInterfaceProvider.GetAllNetworkInterfaces())
                {
                    if(nic.OperationalStatus == OperationalStatus.Up)
                    {
                        var unicastAddresses = nic.GetIPProperties().UnicastAddresses;
                        if(unicastAddresses.Any(ua => ipnetwork.Contains(ua.Address)))
                        {
                            return nic;
                        }
                    }
                }
            }
            else
            {
                IPAddress address = IPAddress.Parse(mask);
                foreach (NetworkInterface nic in networkInterfaceProvider.GetAllNetworkInterfaces())
                {
                    var unicastAddresses = nic.GetIPProperties().UnicastAddresses;
                    if(unicastAddresses.Select(ua => ua.Address).Contains(address))
                    {
                        if(nic.OperationalStatus == OperationalStatus.Up)
                        {
                            return nic;
                        }
                    }
                }
            }
            throw new NetworkInterfaceNotFoundException($"No network interface found for IP address/CIDR Mask {mask}");
        }

    }

	public class NetworkInterfaceProvider
	{
		public virtual NetworkInterface[] GetAllNetworkInterfaces()
		{
			return NetworkInterface.GetAllNetworkInterfaces();
		}
	}
}