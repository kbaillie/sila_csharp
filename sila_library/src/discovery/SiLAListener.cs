﻿namespace sila2.Discovery
{
    using System.Threading;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using System.Text;
    using Tmds.MDns;
    using Common.Logging;

    /// <summary>
    /// This class starts a new thread that is listening to SiLA2 Server Discovery requests and answers them according to the standard.
    /// </summary>
    class SiLAListener
    {
        private static ILog _Logger = LogManager.GetLogger<SiLAListener>();

        /// <summary>
        /// Starts new thread that is listening for mDNS requests.
        /// </summary>
        public void StartListening(string instanceName, string hostName, ushort portNumber, NetworkInterface networkInterface)
        {
            _instanceName = instanceName;
            _hostName = hostName;
            _portNumber = portNumber;
            _currentNetworkInterface = networkInterface;

            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                SiLAmDNS();
                _Logger.Info("Start listening for SiLA2 Server Discovery requests (Service name = \"" + GetQuallifiedServiceName() + "\"");

                while(!stop)
                {
                    Thread.Sleep(100);
                }
                _listenSocket.Close();
                _Logger.Info("[SiLAListener] stopped");
            }).Start();
        }

        public void Stop() {
            _Logger.Info("Stop!");
            stop = true;
        }

        private Socket _listenSocket;

        private static string _hostName = "n/a";
        private string _instanceName = "n/a";
        private static string _serviceName = "_sila._tcp.";
        private static string _domainName = "local.";
        private static ushort _portNumber = 0;
        private static readonly IPAddress mDNSAddress = new IPAddress(new byte[] { 224, 0, 0, 251 });
        private static int mDNSPort = 5353;
        private readonly byte[] _receiveBuffer = new byte[4096];
        private readonly string receiverLock = "";
        private NetworkInterface _currentNetworkInterface = null;
        public bool stop = false;

        /// <summary>
        /// Open sockets for mDNS on a given netowork interface
        /// </summary>
        void SiLAmDNS()
        {
            int index = _currentNetworkInterface.GetIPProperties().GetIPv4Properties().Index;
            _listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _listenSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            _listenSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastInterface, IPAddress.HostToNetworkOrder(index));
            _listenSocket.Bind(new IPEndPoint(IPAddress.Any, mDNSPort));
            _listenSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(mDNSAddress, index));
            _listenSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 1);

            _listenSocket.BeginReceive(_receiveBuffer, 0, _receiveBuffer.Length, SocketFlags.None, OnReceive, null);
        }

        private void OnReceive(IAsyncResult ar)
        {
            lock (receiverLock)
            {
                int length;
                try
                {
                    if (_listenSocket == null)
                    {
                        return;
                    }

                    length = _listenSocket.EndReceive(ar);
                }
                catch (Exception)
                {
                    return;
                }

                var stream = new MemoryStream(_receiveBuffer, 0, length);
                var reader = new DnsMessageReader(stream);
                bool validPacket = true;

                //_packetServiceInfos.Clear();
                //_packetHostAddresses.Clear();

                try
                {
                    Header header = reader.ReadHeader();

                    if (header.IsQuery && header.AnswerCount == 0)
                    {
                        for (int i = 0; i < header.QuestionCount; i++)
                        {
                            Question question = reader.ReadQuestion();
                            Name serviceName = question.QName;

                            //if (question.QType == RecordType.PTR && serviceName.ToString() == serviceQName)
                            if (question.QType == RecordType.PTR && GetQuallifiedServiceName().Contains(serviceName.ToString()))
                            {
                                _Logger.Trace("Matching service query received: " + serviceName);
                                WriteServerDiscoveryAnswer(header, question);
                            }
                        }
                    }
                }
                catch
                {
                    validPacket = false;
                    throw;
                }

                if (validPacket)
                {
                    //HandlePacketHostAddresses();
                    //HandlePacketServiceInfos();
                }

                _listenSocket.BeginReceive(_receiveBuffer, 0, _receiveBuffer.Length, SocketFlags.None, OnReceive, null);
            }
        }
        /*
         * From zeroconf project (part of RFC
         *
            4.1.1. Header section format

            The header contains the following fields:

                                                1  1  1  1  1  1
                  0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
                |                      ID                       |
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
                |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
                |                    QDCOUNT                    |
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
                |                    ANCOUNT                    |
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
                |                    NSCOUNT                    |
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
                |                    ARCOUNT                    |
                +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+

                where:

                ID A 16 bit identifier assigned by the program that
                   generates any kind of query.This identifier is copied
                   the corresponding reply and can be used by the requester
                   to match up replies to outstanding queries.


        QR A one bit field that specifies whether this message is a
                   query (0), or a response(1).

                OPCODE A four bit field that specifies kind of query in this

                                message.This value is set by the originator of a query
                              and copied into the response.The values are:

                                0               a standard query (QUERY)

                                1               an inverse query (IQUERY)

                                2               a server status request (STATUS)

                                3-15            reserved for future use


              AA Authoritative Answer - this bit is valid in responses,
                              and specifies that the responding name server is an
                              authority for the domain name in question section.


                              Note that the contents of the answer section may have

                              multiple owner names because of aliases.  The AA bit
                              corresponds to the name which matches the query name, or
                              the first owner name in the answer section.

              TC TrunCation - specifies that this message was truncated
                              due to length greater than that permitted on the

                              transmission channel.


              RD Recursion Desired - this bit may be set in a query and
                                is copied into the response.  If RD is set, it directs

                              the name server to pursue the query recursively.

                              Recursive query support is optional.

              RA Recursion Available - this be is set or cleared in a
                              response, and denotes whether recursive query support is

                              available in the name server.

              Z Reserved for future use.  Must be zero in all queries

                              and responses.


              RCODE Response code - this 4 bit field is set as part of

                              responses.The values have the following
                              interpretation:

                                0               No error condition

                                1               Format error - The name server was

                                              unable to interpret the query.

                                2               Server failure - The name server was

                                              unable to process this query due to a

                                              problem with the name server.

                                3               Name Error - Meaningful only for

                                              responses from an authoritative name
                                              server, this code signifies that the

                                              domain name referenced in the query does
                                              not exist.

                                4               Not Implemented - The name server does

                                              not support the requested kind of query.

                                5               Refused - The name server refuses to
                                              perform the specified operation for

                                              policy reasons.  For example, a name

                                              server may not wish to provide the
                                              information to the particular requester,
                                              or a name server may not wish to perform
                                              a particular operation (e.g., zone
                                              transfer) for particular data.

                                6-15            Reserved for future use.


              QDCOUNT an unsigned 16 bit integer specifying the number of

                              entries in the question section.

              ANCOUNT an unsigned 16 bit integer specifying the number of

                              resource records in the answer section.

              NSCOUNT an unsigned 16 bit integer specifying the number of name
                              server resource records in the authority records
                              section.


              ARCOUNT an unsigned 16 bit integer specifying the number of

                              resource records in the additional records section.

                */

        private void WriteServerDiscoveryAnswer(Header questionHeader, Question question)
        {
            Name instanceName = new Name(_instanceName + "." + question.QName.ToString());
            DnsMessageWriter writer = new DnsMessageWriter();
            writer.WriteAnswerHeader(questionHeader);

            writer.WritePtrRecord(RecordSection.Answer, instanceName, new Name(_serviceName + _domainName), (uint)120);

            writer.WriteSrvRecord(RecordSection.Answer, instanceName, (uint)120, (ushort)1, (ushort)1, _portNumber, new Name(_hostName + _domainName));

            writer.WriteRecordStart(RecordSection.Answer, new Name(_hostName + _domainName), RecordType.A, (uint)120, RecordClass.Internet);
            var ipAddress = GetIPv4Address(_currentNetworkInterface.GetIPProperties());
            writer.WriteRecordData(ipAddress.GetAddressBytes());
            writer.WriteRecordEnd();

            //
            // Write a TXT record. It must be writte even there are no additional information to write
            //
            List<string> txtList = new List<string>();
            txtList.Add("SerialNumber=1234567");
            writer.WriteTxtRecord(RecordSection.Answer, new Name(_instanceName + "." + question.QName.ToString()), (uint)120, txtList);

            var packets = writer.Packets;
            ////Dump(packets);
            IPEndPoint querier = new IPEndPoint(mDNSAddress, mDNSPort);
            Send(packets, querier);
        }

        internal void Send(IList<ArraySegment<byte>> packets, IPEndPoint endPoint)
        {
            try
            {
                foreach (ArraySegment<byte> segment in packets)
                {
                    _listenSocket.SendTo(segment.Array, segment.Offset, segment.Count, SocketFlags.None, endPoint);
                }
            }
            catch
            {
            }
        }

        #region Helper methods

        internal static void Dump(IList<ArraySegment<byte>> packets)
        {
            try
            {
                int segmentIndex = 0;
                foreach (ArraySegment<byte> segment in packets)
                {
                    _Logger.Info("Sent segment " + segmentIndex++);
                    for (int index = 0; index < segment.Count; index++)
                    {
                        _Logger.Info(string.Format("{0:X02} ", segment.Array[index]));
                        if ((index + 1) % 10 == 0)
                        {
                            _Logger.Info("\t" + Encoding.UTF8.GetString(segment.Array, index - 9, 10).Replace("\r", "\\r").Replace("\n", "\\n"));
                        }
                        else if (index + 1 == segment.Count)
                        {
                            var bytesWritten = (index + 1) % 10;
                            _Logger.Info(new String(' ', (10 - bytesWritten) * 3) + "\t" + Encoding.UTF8.GetString(segment.Array, index - bytesWritten, 10 - bytesWritten).Replace("\r", "\\r").Replace("\n", "\\n"));
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private string GetQuallifiedServiceName()
        {
            return string.Format("{0}.{1}{2}", _instanceName, _serviceName, _domainName);
        }

        /// <summary>
        /// Gets the IPv4 Address of the interface
        /// </summary>
        /// <param name="iPInterfaceProperties">Properties of the network interface</param>
        /// <returns>The IPV4 IP address of the interface</returns>
        private IPAddress GetIPv4Address(IPInterfaceProperties iPInterfaceProperties)
        {
            foreach (var ip in iPInterfaceProperties.UnicastAddresses)
            {
                if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.Address;
                }
            }
            throw new Exception("Could not find IPv4 addres for the interface");
        }

        #endregion
    }
}