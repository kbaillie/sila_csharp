namespace sila2.server_config
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class ServerConfig
    {
        /// <summary>
        /// Configurable name of the server
        /// </summary>
        [DataMember]
        public string Name;
        /// <summary>
        /// Guid of the server, loaded from config or generated
        /// </summary>
        [DataMember]
        public readonly Guid Uuid;

        public ServerConfig(string name, Guid uuid)
        {
            Name = name;
            Uuid = uuid;
        }
    }
}