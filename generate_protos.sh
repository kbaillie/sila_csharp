#!/usr/bin/env bash

# Uncomment this line for debugging
# set -x

SCRIPT_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

CODE_GENERATOR_JAR=${SCRIPT_PATH}/code_generator.jar
PROTOC_SETUP=${SCRIPT_PATH}/protoc_setup.sh

SILA_LIBRARY_PATH=${SCRIPT_PATH}/sila_library
SILA_CORE_FEATURE_PATH=${SCRIPT_PATH}/sila_base/feature_definitions/org/silastandard/core
PROTO_PATH=${SILA_LIBRARY_PATH}/src/protos
STUBS_PATH=${SILA_LIBRARY_PATH}/src/stubs

FEATURE_NAME=SiLAService

# Generate proto from feature
java -jar ${CODE_GENERATOR_JAR} ${SILA_CORE_FEATURE_PATH}/${FEATURE_NAME}.xml ${PROTO_PATH}/${FEATURE_NAME}.proto

. ${PROTOC_SETUP}

# generate stubs for FluentController implementation
${PROTOC} ${PROTO_PATH}/${FEATURE_NAME}.proto  --proto_path ${PROTO_PATH} ${FRAMEWORK_PATH} --csharp_out ${STUBS_PATH} --grpc_out ${STUBS_PATH} --plugin=${CSHARP_PLUGIN}