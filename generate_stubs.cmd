call protoc_setup.cmd

@rem generate stubs for SiLAFramework and SiLA2Device
%PROTOC% sila_base/protobuf/SiLAFramework.proto --proto_path sila_base/protobuf --csharp_out sila_library/src/stubs --grpc_out sila_library/src/stubs --plugin=%CSHARP_PLUGIN%
%PROTOC% sila_library/src/protos/SiLAService.proto --proto_path sila_library/src/protos %FRAMEWORK_PATH% --csharp_out sila_library/src/stubs --grpc_out sila_library/src/stubs --plugin=%CSHARP_PLUGIN%

@rem generate stubs for FirstExample implementation
%PROTOC% sila_implementations/FirstExample/protos/GreetingProvider.proto --proto_path sila_implementations/FirstExample/protos %FRAMEWORK_PATH% --csharp_out sila_implementations/FirstExample/stubs  --grpc_out sila_implementations/FirstExample/stubs --plugin=%CSHARP_PLUGIN%
%PROTOC% sila_implementations/FirstExample/protos/TemperatureController.proto --proto_path sila_implementations/FirstExample/protos %FRAMEWORK_PATH% --csharp_out sila_implementations/FirstExample/stubs --grpc_out sila_implementations/FirstExample/stubs --plugin=%CSHARP_PLUGIN%

@rem generate stub for DataTypeProvider implementation
%PROTOC% sila_implementations/DataTypeProvider/protos/DataTypeProvider.proto --proto_path sila_implementations/DataTypeProvider/protos %FRAMEWORK_PATH% --csharp_out sila_implementations/DataTypeProvider/stubs  --grpc_out sila_implementations/DataTypeProvider/stubs --plugin=%CSHARP_PLUGIN%

@rem generate stubs for MockShaker implementation
%PROTOC% sila_implementations/MockShaker/protos/ShakingControl.proto --proto_path sila_implementations/MockShaker/protos %FRAMEWORK_PATH% --csharp_out sila_implementations/MockShaker/features/stubs  --grpc_out sila_implementations/MockShaker/features/stubs --plugin=%CSHARP_PLUGIN%
%PROTOC% sila_implementations/MockShaker/protos/PlateHandlingControl.proto --proto_path sila_implementations/MockShaker/protos %FRAMEWORK_PATH% --csharp_out sila_implementations/MockShaker/features/stubs --grpc_out sila_implementations/MockShaker/features/stubs --plugin=%CSHARP_PLUGIN%
